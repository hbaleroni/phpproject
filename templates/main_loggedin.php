<span>Welcome <?php echo $_SESSION['user']['username']; ?></span>
<a href='/create_post'>Create New</a>
<a href='/logout'>Logout</a>

<?php foreach($messages as $message): ?>
	<p>
	Message: <?php echo $message['message']; ?>
	<br />
	Author: <?php echo $message['username']; ?>
	</p>

	<?php if ($_SESSION['user']['id'] == $message['user_id']): ?>
		<a href="/update_post/<?php echo $message['id']; ?>">Edit</a>
		<a href="/delete_by_id/<?php echo $message['id']; ?>">Delete</a>
	<?php endif; ?>

	<hr />
<?php endforeach; ?>