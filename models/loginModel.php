<?php

	class loginModel {

		public function login($username, $password, $database) {

			$statement = $database->prepare('SELECT id, username, password
												FROM users
												WHERE username = :username
												AND password = :password
											');
			$statement->execute(array(
				'username' => $username,
				'password' => $password
			));

			$user = $statement->fetch();

			return $user;

		}

		public function read_by_id($id, $database){

			$statement = $database->prepare('SELECT id, username, password
												FROM users
												WHERE id = :id
											');

			$statement->execute(array(
				'id' => $id
			));

			$user = $statement->fetch();

			return $user;

		}

		public function create_new_user($post, $database) {

			$statement = $database->prepare('INSERT INTO users
												(username, password)
												values
												(:username, :password)
											');
			$success = $statement->execute(array(
				'username' => $post['username'],
				'password' => $post['password']
			));

			return $success;
		}
	}
