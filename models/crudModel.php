<?php

	class crudModel {

		public function create($post, $database) {

			// validate
			if (!isset($post['message']) || $post['message'] == null || $post['message'] == '') {
				return false;
			}

			$statement = $database->prepare('INSERT INTO messages
			   	   								(user_id, message)
			   	   								values
			   	   								(:user_id, :message)
   	   										');

			$success = $statement->execute(array(
				'user_id' => $post['user_id'],
				'message' => $post['message']
			));

			return $success;

		}

		public function read_all($database) {

			$statement = $database->prepare('SELECT id, user_id, message
												FROM messages
											');

			$statement->execute();

			$messages = $statement->fetchAll();

			return $messages;

		}

		public function read_by_id($id, $database) {

			$statement = $database->prepare('SELECT id, user_id	, message
												FROM messages
												WHERE id = :id
											');

			$statement->execute(array(
				'id' => $id
			));

			$message = $statement->fetch();

			return $message;

		}

		public function update_by_id($post ,$database) {

			$postOld = $this->read_by_id($post['id'], $database);

			if($postOld['user_id'] != $_SESSION['user']['id']){
				// then someone is hacking
				return false;
			}

			$statement = $database->prepare('UPDATE messages
												SET message = :message
												WHERE id = :id
											');

			$success = $statement->execute(array(
				'message' => $post['message'],
				'id' => $post['id']
			));

			return $success;

		}

		public function delete_by_id($id ,$database) {

			$post = $this->read_by_id($id, $database);

			if($post['user_id'] != $_SESSION['user']['id']){
				// then someone is hacking
				return false;
			}

			$statement = $database->prepare('DELETE
							   					FROM messages
							   					WHERE id = :id
						   					');

			$success = $statement->execute(array(
				'id' => $id
			));

			return $success;

		}

	}