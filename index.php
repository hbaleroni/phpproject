<?php
require 'vendor/autoload.php';
require 'models/crudModel.php';
require 'models/loginModel.php';

session_cache_limiter(false);
session_start();

$app = new Slim\Slim();

// DB variables
$user = "root";
$pass = "root";
$db = new PDO('mysql:host=localhost;dbname=ASL', $user, $pass);

// Landing page/root
$app -> get('/', function() use ($app, $db) {

	$crudModel = new crudModel();
	$loginModel = new loginModel();

	$messages = $crudModel -> read_all($db);

	// attach the username to the message, didnt want to deal with a join table
	for ($i=0; $i < count($messages); $i++) {
		$user = $loginModel->read_by_id($messages[$i]['user_id'], $db);
		$messages[$i]['username'] = $user['username'];
	}

	$data = array();
	$data['messages'] = $messages;

	// the user is logged in
	if(isset($_SESSION['user'])){
		$app->render('main_loggedin.php', $data);
	}else{
		$app->render('main_loggedout.php', $data);
	}

});

// User creation
$app->get('/new_user', function() use($app, $db) {
	$app->render('/create_new_user.php');
});

$app->post('/create_new_user',function() use($app, $db) {

	$username = $app->request->params('username');
	$password = $app->request->params('password');

	$post = array(
		'username' => $username,
		'password' => $password
	);

	$loginModel = new loginModel();
	$loginModel->create_new_user($post, $db);

	$app->redirect('/');

});

// Creates a new post from logged in user
$app -> get('/create_post', function() use ($app) {
	$app->render('create_post.php');
});

$app -> post('/create_post', function() use ($app, $db) {

	// validate the user is logged in
	if(!isset($_SESSION['user'])){
		$app->redirect('/');
		return;
	}

	$userId = $_SESSION['user']['id'];
	$message = $app->request->params('message');

	// validate the variables

	$post = array(
		'user_id' => $userId,
		'message' => $message
	);

	$crudModel = new crudModel();
	$post = $crudModel->create($post, $db);

	// check if everything went okay
	if (!$post) {
		$app->redirect('/create_post');
		return;
	}

	$app->redirect('/');

});

// Edits post from a logged in user
$app -> get('/update_post/:id', function($id) use($app, $db) {

	$crudModel = new crudModel();
	$post = $crudModel->read_by_id($id, $db);

	$app -> render('update_post.php', $post);

});

$app -> post('/update_post', function() use($app, $db) {

	// validate the user is logged in
	if(!isset($_SESSION['user'])){
		$app -> redirect('/');
		return;
	}

	$message = $app->request->params('message');
	$id = $app->request->params('id');

	$post = array(
		'id' => $id,
		'message' => $message
	);

	$crudModel = new crudModel();
	$post = $crudModel->update_by_id($post, $db);

	// check if everything went okay
	if (!$post) {
		$app -> redirect('/update_post/' . $id);
		return;
	}

	$app -> redirect('/');

});

// Deleting the messages
$app->get('/delete_by_id/:id', function($id) use($app, $db) {

	// validate the user is logged in
	if(!isset($_SESSION['user'])){
		$app -> redirect('/');
		return;
	}

	$crudModel = new crudModel();
	$post = $crudModel->delete_by_id($id, $db);

	// check if everything went okay
	if (!$post) {
		$app -> redirect('/');
		return;
	}

	$app->redirect('/');

});

// Login/logout stuff
$app->post('/login', function() use ($app, $db) {

	$username = $app->request -> params('username');
	$password = $app->request -> params('password');

	// validate the variables

	$loginModel = new loginModel();
	$user = $loginModel->login($username, $password, $db);

	// check if everything went okay
	if (!$user) {
		$app->redirect('/login_failed');
		return;
	}

	$_SESSION['user'] = $user;

	$app -> redirect('/');

});

$app -> get('/login_failed', function() use ($app) {
	$app->render('login_failed.php');
});

$app -> get('/logout', function() use ($app) {
	session_destroy();
	$app -> redirect('/');
});

$app -> run();
